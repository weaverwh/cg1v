(function($) {
    "use strict"; // Start of use strict

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    })

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    });

    // Initialize WOW.js Scrolling Animations
    new WOW().init();

    if ($('header').height() < (window.pageYOffset + 50)) {
        $('.stepImages').addClass('active');
        $('.subNav').addClass('active');
    } else {
        $('.stepImages').removeClass('active');
        $('.subNav').removeClass('active');
    }

    var subNavItems = $('.subNavItem');
    var headers = $('h2');
    var heights = [];
    var offset;

    for (var i = 0; i < headers.length; i++) {
        heights.push($(headers[i]).offset().top);
    }

    console.log('firing');

    $('#s4-workspace').scroll(function(event){
        console.log('scroll');
        console.log($('#s4-workspace').scrollTop());
        offset = $(window).height() / 6;
        if ($('header').height() < ($('#s4-workspace').scrollTop() + 60)) {
            $('.stepImages').addClass('active');
            $('.subNav').addClass('active');
        } else {
            $('.stepImages').removeClass('active');
            $('.subNav').removeClass('active');
        }
        for (var i = 0; i < headers.length - 1; i++) {
            if ((($('#s4-workspace').scrollTop() + offset) > heights[i]) && (($('#s4-workspace').scrollTop() + offset) < heights[i + 1])) {
                $(subNavItems).removeClass('active');
                $(subNavItems[i]).addClass('active');
            }
        }
    });

})(jQuery); // End of use strict
